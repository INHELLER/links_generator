# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-10 12:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generator', '0003_auto_20181109_1246'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='deletion_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
