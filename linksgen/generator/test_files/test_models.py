from django.test import TestCase, RequestFactory
from django.contrib.auth.models import AnonymousUser, User

from django.urls import reverse

from linksgen.settings import DELETION_TIMEDELTA
from generator.models import Link
# Create your tests here.


class TestModelLink(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='one_user', password='1234')

    def test_create_link(self):
        obj = Link.new_link(self.user, 'youtube.com')
        self.assertTrue(isinstance(obj, Link))

    def test_link_deletion_date(self):
        obj = Link.new_link(self.user, 'youtube.com')
        expected_date = obj.creation_date + DELETION_TIMEDELTA
        self.assertEqual(obj.deletion_date, expected_date)

    def test_link_deletion_update(self):
        obj = Link.new_link(self.user, 'youtube.com')
        old_date = obj.deletion_date
        obj.refresh_save(self.user)
        self.assertNotEqual(obj.deletion_date, old_date)

    def test_link_is_owner(self):
        obj = Link.new_link(self.user, 'youtube.com')
        self.assertTrue(obj.is_owner(self.user))

    def test_link_is_not_owner(self):
        user2 = User.objects.create(username='Another', password='123')
        obj = Link.new_link(user2, 'youtube.com')
        self.assertFalse(obj.is_owner(self.user))

    def test_link_httpiser_workings(self):
        obj = Link.new_link(self.user, 'youtube.com')
        self.assertTrue(obj.orig_url.startswith('http://'))

    def test_edit_link(self):
        old_url = 'http://youtube.com'
        new_url = 'http://something.org'
        obj = Link.new_link(self.user, old_url)
        self.assertEqual(obj.orig_url, old_url)

        obj.set_url_save(self.user, new_url)
        self.assertEqual(obj.orig_url, new_url)


    def test_delete_link_by_obj(self):
        obj = Link.new_link(self.user, 'youtube.com')
        curr_code = obj.url_code
        obj.delete_curr_link_save(self.user)
        self.assertFalse(Link.objects.filter(pk=curr_code).exists())

    def test_delete_link_by_code(self):
        obj = Link.new_link(self.user, 'youtube.com')
        curr_code = obj.url_code
        Link.delete_link_by_code_save(self.user, curr_code)
        self.assertFalse(Link.objects.filter(pk=curr_code).exists())
