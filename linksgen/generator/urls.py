from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^create/', views.create, name='create'),
    url(r'^check/', views.check_input, name='check_in'),
    url(r'^result/', views.check_output, name='check_out'),
    url(r'^mylinks/', views.my_links, name='my_links'),
    url(r'^edit/(?P<pk>[a-zA-Z0-9]{6})/', views.edit, name='edit'),
    url(r'^refresh/(?P<pk>[a-zA-Z0-9]{6})/', views.refresh, name='refresh'),
    url(r'^delete/(?P<pk>[a-zA-Z0-9]{6})/', views.delete, name='delete'),
]