from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from .utils import generate_short, httpise_link
from linksgen.settings import DELETION_TIMEDELTA

# Create your models here.


class Link(models.Model):
    url_code = models.CharField(max_length=20, primary_key=True)
    orig_url = models.URLField(max_length=300)
    creator = models.ForeignKey(User, blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    deletion_date = models.DateTimeField(blank=True, null=True)
    times_used = models.IntegerField(default=0)

    def creation_date_pretty(self):
        return self.creation_date.strftime('%d.%m.%Y | %H:%M')

    def deletion_date_pretty(self):
        return self.deletion_date.strftime('%d.%m.%Y | %H:%M')

    @classmethod
    def new_link(cls, in_user, in_link):
        short_link = generate_short(6)
        while cls.objects.filter(url_code=short_link).exists():
            short_link = generate_short(6)

        curr_link = cls.objects.create(url_code=short_link)
        curr_link.orig_url = httpise_link(in_link)
        curr_link.deletion_date = curr_link.creation_date + DELETION_TIMEDELTA
        if in_user.is_authenticated:
            curr_link.creator = in_user
        curr_link.save()

        return curr_link

    def is_owner(self, in_user):
        return in_user.is_authenticated and in_user == self.creator

    def set_url_save(self, in_user, in_url):
        if not self.is_owner(in_user):
            return False
        self.orig_url = httpise_link(in_url)
        self.save()
        return True

    def refresh_save(self, in_user):
        if not self.is_owner(in_user):
            return False
        self.deletion_date = timezone.datetime.now() + DELETION_TIMEDELTA
        self.save()
        return True

    def delete_curr_link_save(self, in_user):
        if self.is_owner(in_user):
            self.delete()
            self.save()
            return True
        return False

    @classmethod
    def clear_outdated(cls):
        cls.objects.filter(deletion_date__lte=timezone.datetime.now()).delete()

    @classmethod
    def get_link_by_code(cls, in_code):
        if cls.objects.filter(pk=in_code).exists():
            return cls.objects.get(pk=in_code)
        return None

    @classmethod
    def delete_link_by_code_save(cls, in_user, in_code):
        curr_link = cls.get_link_by_code(in_code)
        if curr_link and curr_link.is_owner(in_user):
            curr_link.delete()
            curr_link.save()
            return True

        return False

    @classmethod
    def get_url_by_code(cls, code, count_usage=True):
        if not cls.objects.filter(pk=code).exists():
            return None

        curr_url = cls.objects.get(pk=code)
        if count_usage:
            curr_url.times_used += 1
            curr_url.save()
        return curr_url.orig_url

    @classmethod
    def get_links_of_user(cls, in_user, ordering='-creation_date'):
        return cls.objects.filter(creator=in_user).order_by(ordering)
