from django.shortcuts import render, redirect
from django.utils import timezone

from django.contrib.sites.shortcuts import get_current_site

import datetime

from .models import Link
from .utils import generate_short, httpise_link

# Create your views here.


def home(request):
    return render(request, 'generator/home.html')


def create(request):
    if request.method == 'GET' or not request.POST['url']:
        return render(request, 'generator/create.html', {'error': 'Invalid url!'})

    curr_site = get_current_site(request)
    Link.clear_outdated()
    curr_result = Link.new_link(request.user, request.POST['url'])
    return render(request, 'generator/create.html', {'curr_result':curr_result, 'site':curr_site})


def check_input(request):
    return render(request, 'generator/checkin.html')


def check_output(request):
    if request.method != 'POST' or not request.POST['code']:
        return redirect('check_in')

    curr_site = get_current_site(request)
    curr_result = Link.get_link_by_code(request.POST['code'])
    if curr_result:
        return render(request, 'generator/checkout.html', {'curr_result': curr_result, 'site':curr_site})

    return render(request, 'generator/checkout.html', {'error': 'Unknown url code.'})




def use_link(request, pk):
    curr_link = Link.get_link_by_code(pk)
    if curr_link:
        return redirect(curr_link)

    return redirect('home')


def my_links(request):
    curr_site = get_current_site(request)
    links = Link.get_links_of_user(request.user)
    return render(request, 'generator/mylinks.html', {'links':links, 'site':curr_site})


def edit(request, pk):
    curr_link = Link.get_link_by_code(pk)
    if not (curr_link and curr_link.is_owner(request.user)):
        return redirect('home')
    elif not curr_link:
        return redirect('my_links')

    if request.method == 'GET':
        return render(request, 'generator/edit.html', {'curr_link':curr_link})
    elif request.POST['url'] == '':
        return render(request, 'generator/edit.html', {'curr_link': curr_link, 'error': 'You entered blank address!'})

    curr_link.set_url_save(request.user, request.POST['url'])
    return redirect('my_links')



def refresh(request, pk):
    if request.method == 'GET' or not request.user.is_authenticated:
        return redirect('home')

    curr_link = Link.get_link_by_code(pk)
    if curr_link and curr_link.refresh_save(request.user):
        return redirect('my_links')

    return redirect('my_links')


def delete(request, pk):
    curr_link = Link.get_link_by_code(pk)
    if curr_link and request.method == 'GET':
        return render(request, 'generator/delete.html', {'curr_link': curr_link})
    elif not (curr_link and request.user.is_authenticated):
        return redirect('home')
    elif not curr_link:
        return redirect('my_links')

    curr_link.delete_curr_link_save(request.user)
    return redirect('my_links')
