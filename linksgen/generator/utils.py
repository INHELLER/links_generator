
import random
import string


def generate_short(size=6, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def httpise_link(in_str):
    if in_str.startswith('http://') or in_str.startswith('https://'):
        return in_str
    return 'http://{}'.format(in_str)
