from django.conf.urls import url, include
from django.contrib import admin

from . import views


urlpatterns = [
    url(r'^signup/', views.signup, name='signup'),
    url(r'^login/', views.login_v, name='login'),
    url(r'^logout/', views.logout_v, name='logout'),
]