from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.


def signup(request):
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:

            try:
                c_user = User.objects.get(username=request.POST['username'])
                return render(request, 'accounts/signup.html', {'error':'Username has alredy taken!'})
            except User.DoesNotExist:
                c_user = User.objects.create_user(username=request.POST['username'],
                                                  password=request.POST['password1'])
                login(request, c_user)
                return redirect('home')
        else:
            return render(request, 'accounts/signup.html', {'error':'Passwords does not match.'})
    else:
        return render(request, 'accounts/signup.html')


def login_v(request):
    if request.method == 'POST':
        c_user = authenticate(username=request.POST['username'],
                              password=request.POST['password'])

        if c_user is not None:
            login(request, c_user)
            if 'next' in request.POST:
                return redirect(request.POST['next'])
            return redirect('home')
        else:
            return render(request, 'accounts/login.html', {'error':'Username or Password didn\'t match.'})
    else:
        return render(request, 'accounts/login.html')


def logout_v(request):
    if request.method == 'POST':
        logout(request)
    return redirect('home')
